package gallery_microservice;
import org.json.JSONArray;
import org.json.JSONObject;
import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class ViewGallery implements Command {
	
	private PostgresService db;
	
	public ViewGallery() {
		db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();
		res.put("status", "200");
		try {
			JSONArray temp = viewGallery(request);
			if (temp!=null) {
				resBody.put("user_msg", "Fetched gallery.");
				resBody.put("result", temp);
			} else {
				resBody.put("user_msg", "Failed to favorite gallery.");
				resBody.put("error", "Database not updated.");
				res.put("status", "400");
			}
		} catch (Exception e) {
			resBody.put("user_msg", "Failed to fetch gallery.");
			resBody.put("error", e.getMessage());
			res.put("status", "400");
		}
		res.put("message", resBody);
		return res;
	}
	
	public JSONArray viewGallery(JSONObject request) throws Exception {
		JSONArray q = new JSONArray();
		try {
			String user_id = request.getString("user_id");
			String sql = "select view_galleries ('%s')";
			String resSQL = String.format(sql, user_id);
			q = db.execute(resSQL).getJSONArray("Result");
			return q;
		} catch (Exception e) {
			throw e;
		}
	}

}
