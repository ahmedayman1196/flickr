package gallery_microservice;

import org.json.JSONArray;
import org.json.JSONObject;
import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class ViewPosts implements Command {
	
	PostgresService db;
	
	public ViewPosts() {
		db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();
		res.put("status", "200");
		try {
			JSONArray temp = viewPost(request);
			if (temp!=null) {
				resBody.put("user_msg", "Fetched post.");
				resBody.put("result", temp);
			} else {
				resBody.put("user_msg", "Failed to get post.");
				resBody.put("error", "Database not updated.");
				res.put("status", "400");
			}
		} catch (Exception e) {
			resBody.put("user_msg", "Failed to fetch Post.");
			resBody.put("error", e.getMessage());
			res.put("status", "400");
		}
		res.put("message", resBody);
		return res;
	}
	
	public JSONArray viewPost(JSONObject album) throws Exception {
		JSONArray q = new JSONArray();
		try {
			String gallery_id = album.getString("gallery_id");
			String sql = "select view_gallery_posts ('%s')";
			String resSQL = String.format(sql, gallery_id);
			System.out.println(db.execute(resSQL));
			q = db.execute(resSQL).getJSONArray("Result");
			System.out.println(q);
			return q;
		} catch (Exception e) {
			throw e;
		}

	}

}
