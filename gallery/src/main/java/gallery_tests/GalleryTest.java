package gallery_tests;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import configuration.Configurations;
import gallery_microservice.*;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;


public class GalleryTest {
	
	
	@Test
	public void test() throws Exception {
		Configurations.defaultConfig();
		PostgresService db = ServiceLocator.getInstance().getServiceByName("pg");

		JSONObject req = new JSONObject();
		JSONObject user = new JSONObject();
		req.put("user_id", "1");
		req.put("name", "dummy gallery");
		req.put("description","dummy desc");
		CreateGallery cg = new CreateGallery();
		JSONObject res= cg.run(req);
		Assert.assertEquals("200", res.getString("status"));
	
		
//		//test add post
		req = new JSONObject();
		req.put("post_id","111");
		req.put("gallery_id","1");
		AddPost ap = new AddPost();
		res= ap.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
		req = new JSONObject();
		
		//test delete post
		
		req = new JSONObject();
		req.put("post_id","111");
		req.put("gallery_id","1");
		DeletePost dp = new DeletePost();
		
		res= dp.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
		
		//test favorite gallery 
		req = new JSONObject();
		req.put("user_id","1");
		req.put("gallery_id","1");
		FavoriteGallery fg = new FavoriteGallery();
		
		res= fg.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
		//view favorite gallery
		
		req = new JSONObject();
		req.put("user_id","1");
		ViewFavorite vf = new ViewFavorite();
		
		res= vf.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
		
		
//		test search gallery
		
		
		req = new JSONObject();
		req.put("name","dummy gallery");
		SearchTitle sg = new SearchTitle();
		
		res= sg.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
		
		
		//test unfavorite gallery 
		
		req = new JSONObject();
		req.put("user_id","1");
		req.put("gallery_id","1");
		UnFavoriteGallery ufg = new UnFavoriteGallery();
		
		res= ufg.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
		
		
		//view galleries
		req = new JSONObject();
		req.put("user_id","1");
		ViewGallery vg = new ViewGallery();
		
		res= vg.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
		//view Posts
		req = new JSONObject();
		req.put("gallery_id","1");
		ViewPosts vp = new ViewPosts();
		
		res= vp.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
		
	}
}
