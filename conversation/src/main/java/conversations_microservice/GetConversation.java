package conversations_microservice;

import org.json.JSONObject;
import org.json.JSONArray;

import com.arangodb.entity.BaseDocument;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;
// FIXME: Not finished at all, should move logic to conversations_microservice
public class GetConversation implements Command {
	PostgresService sql;

	public GetConversation() {
		sql = ServiceLocator.getInstance().getServiceByName("pg");
	}

	@Override
	public JSONObject run(JSONObject request) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();

		try {
			resBody.put("conv_id", getConversation(request));
			res.put("status", "200");
			String resMsg = "Sucessfully fetched messages.";
			resBody.put("user_msg", resMsg);
		} catch (Exception e) {
			res.put("status", "400");
			resBody.put("user_msg", "Failed to send message.");
			resBody.put("error", "Failed to send message.");
		}
		res.put("message", resBody);
		return res;
	}
	// TODO:
	// 1) create message (Arango)
	// 2) Add message to conversation
	public String getConversation(JSONObject request) throws Exception {

		try {
			// TODO: inject auth user id
			// int user_id = request.getInt("user_id");
			int member1_id = request.getInt("member1_id");
			int member2_id = request.getInt("member2_id");
			Map<String, Object> bindVariables = new HashMap<String, Object>();
			String sql_query = "select get_conversation ('%s', '%s')";
			String resSQL = String.format(sql_query, member1_id, member2_id);

			System.out.println(resSQL);
			return (sql.execute(resSQL)).getJSONArray("Result").getJSONObject(0).get("get_conversation")+"";
			
			

		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}

	}

}
