package conversations_microservice;

import org.json.JSONArray;
import org.json.JSONObject;

import com.arangodb.entity.BaseDocument;

import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class CreateConversation implements Command {
	private PostgresService db;
	private ArangoService arango;

	public CreateConversation() {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		db = serviceLocator.getServiceByName("pg");
		arango = serviceLocator.getServiceByName("arango");
	}

	// 1) create conversation
	// 2) add users to conversation
	public JSONObject run(JSONObject request) {
		//Authentication Still needed
		JSONArray users;
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();

		try {
			users = (JSONArray) request.get("users");
			System.out.println(users);
		} catch (Exception e) {
			res.put("message", new JSONObject().put("error", "Missing param(s)"));
			res.put("status", "400");
			return res;
		}
		BaseDocument groupObj = new BaseDocument();
		groupObj.addAttribute("is_deleted", false);

		try {
			String conversation_id = arango.createDocument("Conversations", groupObj);
			System.out.println("conversation_id: "+ conversation_id);
			
			res.put("status", "200");
			resBody.put("conversation_id", conversation_id);
			
			int min = Math.min(users.length(), 10);
			for (int i=0; i<min; i++)
			{
				JSONObject sqlObj = add_user(Integer.parseInt(users.getString(i)), conversation_id);
			}
			String resMsg = "Sucessfully created Conversation with ID: " + conversation_id;
			resBody.put("user_msg", resMsg);
			
		} catch(Exception e) {
			res.put("status", "400");
			resBody.put("user_msg", "Failed to create conversation.");
			resBody.put("error", "Failed to create conversation.");
		}
		
		res.put("message", resBody);
		return res;
	}
	
	public JSONObject add_user(int user_id, String conv_id) throws Exception {
		try {
			
			String sql = "select add_user_to_conversation ('%s', '%s')";
			String resSQL = String.format(sql, user_id, conv_id);
			System.out.println(resSQL);
			return db.execute(resSQL);
		} catch (Exception e) {
			throw e;
		}
	}

}
