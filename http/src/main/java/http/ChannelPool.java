package http;


import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

public class ChannelPool {
	private int channelPoolSize;
	private GenericObjectPool<Channel> channelPool;

	public ChannelPool(int channelPoolSize, Connection connection) {
		this.channelPoolSize = channelPoolSize;
		channelPool = new GenericObjectPool<Channel>(new ChannelFactory(connection), getConfig());

	}

	private GenericObjectPoolConfig getConfig() {
		GenericObjectPoolConfig config = new GenericObjectPoolConfig();
		config.setMaxTotal(channelPoolSize);
		config.setBlockWhenExhausted(true);
		return config;
	}

	public GenericObjectPool<Channel> getChannelPool() {
		return channelPool;
	}
	
	public Channel getChannel() {
			try {
				return channelPool.borrowObject();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			return null;	
	}

	public void returnChannel(Channel channel) {
		if (channel.isOpen())
			channelPool.returnObject(channel);
		else
			try {
				channelPool.invalidateObject(channel);
			} catch (Exception e) {
				e.printStackTrace();
			}

	}
	public void closePool() {
		if(!channelPool.isClosed())
			channelPool.close();
	}
	public int getActive() {
		return channelPool.getNumActive();
	}
	public int getIdle() {
		return channelPool.getNumIdle();
	}
}
