package http;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONObject;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.router.RouteResult;
import io.netty.handler.codec.http.router.Router;
import io.netty.util.CharsetUtil;

@ChannelHandler.Sharable
public class HTTPHandler extends SimpleChannelInboundHandler<Object> {
	private FullHttpRequest request;
	private String requestBody;
	private long correlationId;
	private static ChannelPool channelPool;
	volatile String responseBody;
	ExecutorService executorService = Executors.newCachedThreadPool();
	private final Router<String> router;

	HTTPHandler(Router<String> router) {
		this.router = router;
		Connection connection = MQConnection.getInstance().getConnection();
		channelPool = new ChannelPool(100, connection);
	}

	public void channelReadComplete(ChannelHandlerContext ctx) {
		ctx.flush();
//		ctx.fireChannelReadComplete();
		ctx.close();

	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
		if (msg instanceof FullHttpRequest) {
			FullHttpRequest request = this.request = (FullHttpRequest) msg;
			HttpResponse res = createResponse(request, router);
			System.out.println(res);
//			flushResponse(ctx, request, res);
			ctx.write(res);
//			if (HttpUtil.is100ContinueExpected(request)) {
//				send100Continue(ctx);
//			}

		}
//		if (msg instanceof HttpContent) {
//			HttpContent httpContent = (HttpContent) msg;
//			ByteBuf content = httpContent.content();
////            setRequestBody(content.toString(CharsetUtil.UTF_8));
//			ctx.fireChannelRead(content.copy());
//		}
//		if (msg instanceof LastHttpContent) {
////            LastHttpContent trailer = (LastHttpContent) msg;
////			HttpObject trailer = (HttpObject) msg;
////            writeresponse(trailer, ctx);
//		}
	}

	private static void send100Continue(ChannelHandlerContext ctx) {
		ctx.writeAndFlush(new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.CONTINUE));
		return;
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}

	// Display debug info.
	private static HttpResponse createResponse(FullHttpRequest request, Router<String> router) {
		RouteResult<String> routeResult = router.route(request.method(), request.uri());
		
		ByteBuf buffer = (ByteBuf) request.content();
		JSONObject jsonObject = new JSONObject(buffer.toString(CharsetUtil.UTF_8));
		jsonObject.put("uri", request.uri());
		FullHttpResponse res;
		
//		String content = "router: \n" + router + "\n" + "req: " + request + "\n\n" + "routeResult: \n" + "target: "
//				+ routeResult.target() + "\n" + "pathParams: " + routeResult.pathParams() + "\n" + "queryParams: "
//				+ routeResult.queryParams() + "\n\n" + "allowedMethods: " + router.allowedMethods(request.uri());
//		
//		System.out.println(request.toString());
//
//		FullHttpResponse res = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK,
//				Unpooled.copiedBuffer(content, CharsetUtil.UTF_8));
//
//		res.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/plain");
//		res.headers().set(HttpHeaderNames.CONTENT_LENGTH, res.content().readableBytes());

//			ConnectionFactory factory = new ConnectionFactory();
//			factory.setHost("localhost");
//			Connection connection = factory.newConnection();
//			Channel channel = connection.createChannel();
		Channel channel = channelPool.getChannel();
		try {
			String replyQueueName = channel.queueDeclare().getQueue();
			String requestQueueName = routeResult.target();
			String requestFunctionName = request.uri();
			System.out.println(" [x] Requesting " + requestFunctionName + " from " + requestQueueName + " with ID: ");
			String corrId = UUID.randomUUID().toString();
			System.out.println(corrId);
			AMQP.BasicProperties props = new AMQP.BasicProperties.Builder().correlationId(corrId)
					.replyTo(replyQueueName).build();

			channel.basicPublish("", requestQueueName, props, jsonObject.toString().getBytes("UTF-8"));

			final BlockingQueue<String> response = new ArrayBlockingQueue<String>(1);

			channel.basicConsume(replyQueueName, true, new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
						byte[] body) throws IOException {
					if (properties.getCorrelationId().equals(corrId)) {
						response.offer(new String(body, "UTF-8"));
					}
				}
			});
			String response_string = response.take();
			JSONObject temp = new JSONObject(response_string);
			System.out.println(" [.] Got '" + response_string + "'");
			response_string = temp.get("message").toString();
			ByteBuf content = Unpooled.copiedBuffer(response_string, CharsetUtil.UTF_8);
			switch (temp.get("status").toString()) {
			case "200":
				res = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, content);
				break;
			case "400":
				res = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST, content);
				break;
			case "401":
				res = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.UNAUTHORIZED, content);
				break;
			case "500":
				res = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.INTERNAL_SERVER_ERROR,
						content);
				break;
			default:
				res = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.INTERNAL_SERVER_ERROR,
						content);
				break;
			}
			
		} catch (Exception e) {
			System.out.println(e);
			ByteBuf content = Unpooled.copiedBuffer("{\"error\": \"there's something wrong your request\"}",
					CharsetUtil.UTF_8);
			res = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.INTERNAL_SERVER_ERROR, content);
		} finally {
			channelPool.returnChannel(channel);
		}
		res.headers().set("Content-Type", "application/json");
		return res;
	}

//	private static void flushResponse(ChannelHandlerContext ctx, HttpRequest req, HttpResponse res) {
//		if (!HttpUtil.isKeepAlive(req)) {
//			ctx.writeAndFlush(res).addListener(ChannelFutureListener.CLOSE);
//		} else {
//			res.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
//			ctx.writeAndFlush(res);
//		}
//	}
}
