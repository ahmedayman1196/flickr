package http;

import java.io.IOException;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.router.Router;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import resources.ReadRouter;

public class NettyHTTPServer {
	static ReadRouter rr = new ReadRouter();

	public static void start(int port) {
		Router<String> router = new Router<String>();
		
		try {
			router = rr.getRouter();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
        
		EventLoopGroup bossGroup = new NioEventLoopGroup(1);
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
					.handler(new LoggingHandler(LogLevel.INFO)).childHandler(new HTTPServerInitializer(router));
//            b.option(ChannelOption.SO_KEEPALIVE, true);
			Channel ch = b.bind(port).sync().channel();

			System.err.println("Server is listening on http://127.0.0.1:" + port + '/');
			ch.closeFuture().sync();
		} catch (InterruptedException e) {
			
			e.printStackTrace();

		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}

	public static void main(String[] args) throws Exception {
		NettyHTTPServer.start(3000);
	}
}
