package groups_tests;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import configuration.Configurations;
import groups_microservice.CreateGroup;
import groups_microservice.JoinGroup;
import groups_microservice.LeaveGroup;
import groups_microservice.SearchGroup;
import groups_microservice.ViewGroup;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;


public class GroupsTest {
	
	
	@Test
	public void test() throws Exception {
		Configurations.defaultConfig();
		PostgresService db = ServiceLocator.getInstance().getServiceByName("pg");
		//create dummy user
		String name = "Test user";
		String birthDate = "1995-09-30";
		String username = "tester";
		String email = "helloworld@test.com";
		String mobileNumber = "0123456789";
		String hashedPwd = "helloworld123";
		String gender = "male";
		String sql = "select create_user ('%s', '%s', '%s', '%s', '%s', '%s', '%s')";
		String resSQL = String.format(sql, name, birthDate, username, email, mobileNumber, gender, hashedPwd);
		JSONObject userObj = db.execute(resSQL);
		System.out.println(((JSONObject)((JSONArray)userObj.get("Result")).get(0)).toString());
		String userID = "" + ((JSONObject)((JSONArray)userObj.get("Result")).get(0)).get("create_user");
		
		//test create group
		JSONObject req = new JSONObject();
		JSONObject user = new JSONObject();
		user.put("_id", "dummy user ID");
		req.put("user", user);
		req.put("title", "dummy group");
		CreateGroup cg = new CreateGroup();
		JSONObject res = cg.run(req);
		Assert.assertEquals("200", res.getString("status"));
		String groupID = ((JSONObject)((JSONObject)res.get("message")).get("result")).getString("_id");
		
		//test join group
		req = new JSONObject();
		user = new JSONObject();
		user.put("_id", userID);
		req.put("user", user);
		req.put("group_id", groupID);
		JoinGroup jg = new JoinGroup();
		res = jg.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
		//test leave group
		LeaveGroup lg = new LeaveGroup();
		res = lg.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
		//test search group
		req = new JSONObject();
		req.put("search","dummy group");
		SearchGroup sg = new SearchGroup();
		res = sg.run(req);
		Assert.assertEquals("200", res.getString("status"));
		Assert.assertTrue(((JSONObject)res.get("message")).getJSONArray("result").length()>0);
		
		//test view group
		req = new JSONObject();
		req.put("_id", groupID);
		ViewGroup vg = new ViewGroup();
		res = vg.run(req);
		Assert.assertEquals("200", res.getString("status"));
		Assert.assertEquals(res.getJSONObject("message").getJSONObject("result").getString("admin_id"), "dummy user ID");
		
		//test delete group
		req = new JSONObject();
		user = new JSONObject();
		user.put("_id", "dumy user ID");
		req.put("user", user);
		req.put("_id", groupID);
		Assert.assertEquals("200", res.getString("status"));		
	}
}
