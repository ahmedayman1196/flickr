package configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

public class AppConfig {
	private Properties appConfig;
	
	private AppConfig() {
		appConfig = new Properties();
	}
	
	private AppConfig(Properties props) {
		appConfig = props;
	}
	
	public void addConfigVar(String key, String item) {
		appConfig.put(key, item);
	}
	
	public String getConfigVar(String key) {
		return appConfig.getProperty(key);
	}

	public static AppConfig loadFromEnv() {
		return loadFromEnv(".*");
	}
	
	public static AppConfig loadFromEnv(String regex) {
		AppConfig appConfig = new AppConfig();
		Map<String, String> envMap = System.getenv();
		for(String key : envMap.keySet())
			if(key.matches(regex))
				appConfig.addConfigVar(key, envMap.get(key));
		
		return appConfig;
	}
	
	public static AppConfig loadFromPropsFile(String dir) throws IOException {
		Properties props = new Properties();
		InputStream stream = AppConfig.class.getResourceAsStream(dir);
		props.load(stream);
		return new AppConfig(props);
	}
}
