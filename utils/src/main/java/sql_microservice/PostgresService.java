package sql_microservice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.dbcp.BasicDataSource;
import javax.sql.DataSource;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class PostgresService {
	private String url;
	private String user;
	private String password;
	private String driverName;
	private int initialPoolSize;
	private DataSource dataSource;
	private int maxPoolSize;

	public PostgresService(String url, String username, String password
			, String driverName, int initialPoolSize, int maxPoolSize){
		this.url = url;
		this.user = username;
		this.password = password;
		this.driverName = driverName;
		this.initialPoolSize = initialPoolSize;
		this.maxPoolSize = maxPoolSize;
	}

	public void connect() {
		BasicDataSource ds = new BasicDataSource();
		ds.setUrl(this.url);
		ds.setUsername(this.user);
		ds.setPassword(this.password);
		ds.setInitialSize(this.initialPoolSize);
		ds.setDriverClassName(this.driverName);
		ds.setMaxActive(this.maxPoolSize);
		this.dataSource = ds;
	}

	public JSONObject execute(String query) throws SQLException {
		Connection conn = null;
		try {
			conn = this.dataSource.getConnection();
			conn.setAutoCommit(true);
			Statement st = conn.createStatement();
			// Turn use of the cursor on.
			st.setFetchSize(10);
			ResultSet rs = st.executeQuery(query);
			JSONObject jo = new JSONObject();
			jo.put("Result", convert(rs));
			rs.close();
			st.close();
			conn.close();
			return jo;
		} catch (Exception e) {
			System.out.print(e.getMessage());
		} finally {
			if(conn != null)
				conn.close();
		}
		return null;
	}
	
	public ResultSet executeStatementWithParams(String query, Object... params) throws SQLException
	{
		Connection conn = null;
		try {
			conn = this.dataSource.getConnection();
			PreparedStatement sqlStatement = conn.prepareStatement(query);
			parameterizeStatement(sqlStatement, params);
			ResultSet resultSet = sqlStatement.executeQuery();
			resultSet.close();
			sqlStatement.close();
			return resultSet;
		} finally {
			conn.close();
		}
	}
	
	private void parameterizeStatement(PreparedStatement statement, Object... params) throws SQLException
	{
		for(int i = 0; i < params.length; i++)
		{
			Object arg = params[i];
			if (arg instanceof String)
				statement.setString(i+1, (String) arg);
			else if (arg instanceof Integer)
				statement.setInt(i+1, (Integer) arg);
			else if (arg instanceof Date)
				statement.setTimestamp(i+1, new Timestamp(((Date) arg).getTime()));
			else if (arg instanceof Long)
				statement.setLong(i+1, (Long) arg);
			else if (arg instanceof Double)
				statement.setDouble(i+1, (Double) arg);
			else if (arg instanceof Float)
				statement.setFloat(i+1, (Float) arg);
		}
	}
	
	public JSONArray convert(ResultSet rs) throws SQLException, JSONException {
		JSONArray json = new JSONArray();
		ResultSetMetaData rsmd = rs.getMetaData();
		while (rs.next()) {
			int numColumns = rsmd.getColumnCount();
			JSONObject obj = new JSONObject();
			for (int i = 1; i < numColumns + 1; i++) {
				String column_name = rsmd.getColumnName(i);

				if (rsmd.getColumnType(i) == java.sql.Types.BOOLEAN) {
					obj.put(column_name, rs.getBoolean(column_name));
				} else if (rsmd.getColumnType(i) == java.sql.Types.DOUBLE) {
					obj.put(column_name, rs.getDouble(column_name));
				} else if (rsmd.getColumnType(i) == java.sql.Types.FLOAT) {
					obj.put(column_name, rs.getFloat(column_name));
				} else if (rsmd.getColumnType(i) == java.sql.Types.INTEGER) {
					obj.put(column_name, rs.getInt(column_name));
				} else if (rsmd.getColumnType(i) == java.sql.Types.NVARCHAR) {
					obj.put(column_name, rs.getNString(column_name));
				} else if (rsmd.getColumnType(i) == java.sql.Types.VARCHAR) {
					obj.put(column_name, rs.getString(column_name));
				} else if (rsmd.getColumnType(i) == java.sql.Types.TINYINT) {
					obj.put(column_name, rs.getInt(column_name));
				} else if (rsmd.getColumnType(i) == java.sql.Types.SMALLINT) {
					obj.put(column_name, rs.getInt(column_name));
				} else if (rsmd.getColumnType(i) == java.sql.Types.DATE) {
					obj.put(column_name, rs.getDate(column_name));
				} else if (rsmd.getColumnType(i) == java.sql.Types.TIMESTAMP) {
					obj.put(column_name, rs.getTimestamp(column_name));
				} else {
					obj.put(column_name, rs.getObject(column_name));
				}
			}
			json.put(obj);
		}
		return json;
	}
}
