package exceptions;

public class ObjectConstructionError extends Exception {
	private static final long serialVersionUID = 1L;

	public ObjectConstructionError(String msg) {
		super(msg);
	}
}
