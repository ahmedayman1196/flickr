	package comment_microservice;

import org.json.JSONObject;
import command.Command;
import comment_microservice.database_acess_layer.CommentDAO;
import service_locator.ServiceLocator;

public class UnlikeComment implements Command {
	private CommentDAO comments;
	
	public UnlikeComment() {
		this.comments = ServiceLocator.getInstance().getServiceByName("comments");
	}
	
	@Override
	public JSONObject run(JSONObject object) {
		JSONObject response = new JSONObject();
		try {
			int commentId = object.getInt("id");
			int userId = object.getInt("user_id");
			comments.removeLikeFromComment(userId, commentId);
			response.put("status", "200");
			response.put("message", new JSONObject().put("result", true));
		} catch(Exception e) {
			e.printStackTrace();
			response.put("status", "500");
			response.put("message", new JSONObject().put("error", "internal server error"));
		}
		return response;
	}
}
