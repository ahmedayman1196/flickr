package comment_microservice.database_acess_layer;

import java.sql.SQLException;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import comment_microservice.entities.Comment;
import comment_microservice.entities.Comment.CommentBuilder;
import comment_microservice.entities.CommentType;
import exceptions.DAOException;
import exceptions.ObjectConstructionError;
import sql_microservice.PostgresService;
import nosql_microservice.*;

public class CommentAccessor implements CommentDAO {
	private PostgresService postgresService;
	private ArangoService arango;
	
	public CommentAccessor(PostgresService postgresService, 
			ArangoService arangoService) {
		this.postgresService = postgresService;
		this.arango = arangoService;
	}

	public Comment getCommentById(int id) throws DAOException {
		try {
			JSONObject commentJson = arango.readDocument("comment", Integer.toString(id));
			JSONObject result = commentJson.getJSONObject("result");
			HashMap<String, Object> resultBody = (HashMap<String, Object>) result.get("map");
			CommentBuilder builder = new CommentBuilder();
			builder.setId(result.getInt("id"))
				.setIsDeleted((boolean) resultBody.get("isDeleted"))
				.setText((String) resultBody.get("text"))
				.setType(CommentType.fromString((String) resultBody.get("type")))
				.setUserId(((Long) resultBody.get("userId")).intValue())
				.setParentId(((Long)resultBody.get("parentId")).intValue());
			return builder.build();
		} catch (JSONException | ObjectConstructionError e) {
			throw new DAOException(e);
		}
	}

	public int createComment(Comment comment) {
		String key = arango.createDocument("comment", comment.toJson());
		return Integer.parseInt(key);
	}

	public void addLikeForComment(int userId, int commentId) throws DAOException {
		try {
			String query = "select like_comment(?, ?)";
			postgresService.executeStatementWithParams(query, commentId, userId);
		}
		catch(SQLException e) {
			throw new DAOException(e);
		}
	}

	public void removeLikeFromComment(int userId, int commentId) throws DAOException {
		try {
			String query = "select unlike_comment(?, ?)";
			postgresService.executeStatementWithParams(query, commentId, userId);
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}
}