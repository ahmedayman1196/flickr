package comment_microservice;

import org.json.JSONObject;
import command.Command;
import comment_microservice.database_acess_layer.CommentDAO;
import comment_microservice.entities.Comment;
import comment_microservice.entities.CommentType;
import service_locator.ServiceLocator;

/*
 *	TODO: need to check for the existence of the parent(gallery, discussion, post)
 *	by possibly delegating it to the corresponding ms in future updates.
 */
public class CreateComment implements Command {
	private CommentDAO comments;
	
	public CreateComment() {
		this.comments = ServiceLocator.getInstance().getServiceByName("comments");
	}
	
	@Override
	public JSONObject run(JSONObject object) {
		JSONObject response = new JSONObject();
		try {
			int userId = object.getInt("id");
			CommentType commentType = CommentType.fromString(object.getString("type"));
			String text = object.getString("text");
			int parentId = object.getInt("parent_id");
			
			Comment comment = (new Comment.CommentBuilder())
					.setUserId(userId)
					.setType(commentType)
					.setText(text)
					.setParentId(parentId)
					.build();
			
			int commentId = comments.createComment(comment);
			response.put("status", "200");
			response.put("message",new JSONObject().put("result", commentId));
		} catch(Exception e) {
			e.printStackTrace();
			response.put("status", "400");
			response.put("message", new JSONObject().put("error", "internal server error."));
		}
		return response;
	}
}
