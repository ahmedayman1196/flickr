package users_microservice;

import cache.CacheService;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mindrot.jbcrypt.BCrypt;

import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

import java.util.HashMap;
import java.util.HashSet;

public class EditUser implements Command {
	private PostgresService db;

	public EditUser() {
		this.db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();
		try {
			res.put("status", "200");
			String resMsg = "Sucessfully updated user.";
			JSONObject sqlObj = editUser(request);
			resBody.put("user_msg", resMsg);
			resBody.put("user", sqlObj);
			// TODO: check for updated rows
		} catch (Exception e) {
			res.put("status", "400");
			resBody.put("user_msg", "Failed to edit user.");
			resBody.put("error", e.getMessage());
		}
		res.put("message", resBody);
		return res;
	}

	public String hashPassword(String password) {
		return BCrypt.hashpw(password, BCrypt.gensalt());
	}

	public JSONObject editUser(JSONObject request) throws Exception {

		try {
			String token = request.getString("token");
			CacheService cache  = ServiceLocator.getInstance().getServiceByName("cache");

			// check if the token exists in the cache
			if (!cache.exists(token)) {
				throw new Exception();
			}

			HashSet<String> userAttributes = new HashSet<>();
			userAttributes.add("id");
			userAttributes.add("name");
			userAttributes.add("birthdate");
			userAttributes.add("mobile_number");
			userAttributes.add("gender");
			userAttributes.add("cover_pic");
			userAttributes.add("profile_pic");

			HashMap<String,String> userUpdatedInfo = new HashMap<>();

			// getting user info from cache
			JWTService jwt = new JWTService();
			JSONObject userInfo = jwt.decode(cache.get(token)) ;


			for (String attribute : userAttributes) {
				String attributeValue = "";
				try {
					attributeValue = request.get(attribute).toString();
					// modify cache value
					userInfo.put(attribute,attributeValue) ;
				} catch (JSONException e1) {
					try {
						attributeValue = userInfo.get(attribute).toString();
					}catch (Exception e2){
						// this attribute is not in cache nor already saved
					}
				}
				userUpdatedInfo.put(attribute, attributeValue);
			}

			String id = userUpdatedInfo.get("id");
			String name = userUpdatedInfo.get("name");
			String birthDate = userUpdatedInfo.get("birthdate");
			String mobileNumber = userUpdatedInfo.get("mobile_number");
			String gender = userUpdatedInfo.get("gender");
			String coverPic = userUpdatedInfo.get("cover_pic");
			String profilePic = userUpdatedInfo.get("profile_pic");


			String sql = "select update_user ('%s', '%s', '%s', '%s', '%s', '%s', '%s')";
			String resSQL = String.format(sql, id, name, birthDate, mobileNumber, gender, coverPic, profilePic);
			JSONObject res = db.execute(resSQL);

			// update cache if query succeeded
			if(res != null){
				cache.set(token,jwt.encode(userInfo.toString()));
			}
			return res ;
		} catch (Exception e) {
			throw e;
		}
	}

}
