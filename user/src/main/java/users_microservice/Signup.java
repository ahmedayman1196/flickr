package users_microservice;

import org.json.JSONObject;
import org.mindrot.jbcrypt.BCrypt;

import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class Signup implements Command {

	private PostgresService db;

	public Signup() {
		this.db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject user) {
		JSONObject res = null;
		res = new JSONObject();
		JSONObject resBody = new JSONObject();
		try {
			res.put("status", "200");
			String resMsg = "Sucessfully registered.";
			JSONObject sqlObj = createUser(user);
			resBody.put("user_msg", resMsg);
			resBody.put("result", sqlObj);
			res.put("token", createJWTToken(user));
		} catch (Exception e) {
			resBody.put("user_msg", "Failed to register.");
			resBody.put("error", e.getMessage());
			res.put("status", "400");
		}
		res.put("message", resBody);
		return res;
	}

	public String hashPassword(String password) {
		return BCrypt.hashpw(password, BCrypt.gensalt());
	}

	public String createJWTToken(JSONObject obj) {
		JWTService jwt = new JWTService();
		return jwt.encode(jwt.toString());
	}

	public JSONObject createUser(JSONObject user) throws Exception {

		try {
			String name = user.getString("name");
			String birthDate = user.getString("birthdate");
			String username = user.getString("username");
			String email = user.getString("email");
			String mobileNumber = user.getString("mobile_number");
			String hashedPwd = hashPassword(user.getString("password"));
			String gender = user.getString("gender");
			String sql = "select create_user ('%s', '%s', '%s', '%s', '%s', '%s', '%s')";
			String resSQL = String.format(sql, name, birthDate, username, email, mobileNumber, gender, hashedPwd);
			JSONObject resObj = db.execute(resSQL);

			if (resObj == null) {
				// FIXME: throw custom exception
				throw (new Exception("User not registered"));
			} else {
				user.remove("password");
				return resObj;
			}
		} catch (Exception e) {
			throw e;
		}

	}
}
