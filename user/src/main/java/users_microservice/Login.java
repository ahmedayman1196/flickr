package users_microservice;

import cache.CacheService;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mindrot.jbcrypt.BCrypt;

import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class Login implements Command {

	private PostgresService db;
	private CacheService cache;

	public Login() {
		this.db = ServiceLocator.getInstance().getServiceByName("pg");
		this.cache = ServiceLocator.getInstance().getServiceByName("cache");
	}

	@Override
	public JSONObject run(JSONObject object) {
		return authenticate(object);
	}

	public JSONObject authenticate(JSONObject req) {
		JSONObject res = new JSONObject();
		res = new JSONObject();
		JSONObject resBody = new JSONObject();
		try {

			JSONObject user = getUser(req.getString("email"));
			JSONArray userArray = (JSONArray) user.get("Result");
			JSONObject singleUser = (JSONObject) userArray.get(0);

			if (checkPassword(req.getString("password"), singleUser.getString("password"))) {
				res.put("status", "200");
				String resMsg = "Sucessfully logged in.";
				resBody.put("user_msg", resMsg);

				JWTService jwt = new JWTService();
				JSONObject encode = new JSONObject();
				singleUser.remove("password");
				encode.put("user", singleUser.get("id"));
				String token = jwt.encode(encode.toString()); // should contain user id only

				// caching all user information
				String singleUserEncoded = jwt.encode(singleUser.toString());
				cache.set(token,singleUserEncoded); // all user data

				// to use the token in any microservice
				JSONObject decoded = jwt.decode(cache.get(token)) ;

				resBody.put("token", token);
				resBody.put("user", singleUser);

			} else {
				res.put("status", "400");
				resBody.put("error", "Invalid credentials");
			}
			res.put("message", resBody);
		} catch (Exception e) {
			resBody.put("user_msg", "Failed to login.");
			resBody.put("error", e.getMessage());

			res.put("message", resBody);
			res.put("status", "400");
		}

		return res;
	}

	public boolean checkPassword(String candidate, String hashedpwd) {
		return BCrypt.checkpw(candidate, hashedpwd);
	}

	public JSONObject getUser(String email) throws Exception {
		try {
			String sql = "SELECT * FROM users WHERE email='%s'";
			JSONObject user = db.execute(String.format(sql, email));
			return user;
		} catch (Exception e) {
			throw e;
		}
	}
}
