package posts_microservice;

import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

import org.json.JSONArray;
import org.json.JSONObject;
import sql_microservice.PostgresService;

import java.util.HashMap;

public class Search implements Command {
    private PostgresService sql_db;
	private ArangoService arango;
	
    public Search() {
    	ServiceLocator serviceLocator = ServiceLocator.getInstance();
    	this.arango = serviceLocator.getServiceByName("arango");
    	this.sql_db = serviceLocator.getServiceByName("pg");
    }

    @Override
    public JSONObject run(JSONObject object) {
        JSONArray result = new JSONArray();
        JSONObject res = new JSONObject();
        try {
            JSONObject queryRes = sql_db.execute(String.format("SELECT * FROM users WHERE difference(s.name, '%s') > 2 ", object.get("term")));
            JSONArray queryArray = (JSONArray) queryRes.get("Result");
            for(Object single_user: queryArray)
            {

                JSONObject usr = (JSONObject) single_user ;
                result.put(usr.getInt("id"));

            }
            HashMap<String, Object> bind_vars = new HashMap<>();
            bind_vars.put("user_ids", result);
            JSONObject postsRes = arango.query("FOR id IN @user_ids" +
                    "LET posts = (" +
                            "FOR p IN Posts" +
                            " FILTER id == p.user_id" +
                            "  RETURN r" +
                            "  )" + " RETURN posts"
                    ,bind_vars);
//                    "  RETURN { FOR p IN Posts FILTER p.id IN @post_ids RETURN p }",

            res.put("status", "200");
            res.put("message",new JSONObject().put("result", postsRes.get("result")));
        } catch(Exception e) {
            res.put("status", "500");
            res.put("message",new JSONObject().put("error", "Failed to retrieve favorited posts."));
        }
        return res;
    }
}
