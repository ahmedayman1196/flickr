package posts_microservice;

import java.util.HashMap;

import org.json.JSONObject;

import com.arangodb.entity.BaseDocument;

import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

public class Report implements Command {
	private ArangoService arango;
	
	public Report() {
		this.arango = ServiceLocator.getInstance().getServiceByName("arango");
	}
	@Override
	public JSONObject run(JSONObject object) {
        JSONObject res = new JSONObject();
        HashMap<String, Object> params = new HashMap<>();
        params.put("user_id", object.get("user_id"));
        params.put("text", object.get("text"));
        params.put("model", object.get("model"));
        params.put("model_id", object.get("model_id"));
        try {
        	HashMap<String, Object> queryParams = new HashMap<>();
        	queryParams.put("post_id", object.get("model_id"));
        	JSONObject queryRes = arango.query("FOR p IN Posts FILTER p._key == @post_id RETURN p", queryParams);
        	if(queryRes.getJSONArray("result").length() > 0)
        	{
        		BaseDocument reportObject = new BaseDocument();
                params.forEach((key,value)->reportObject.addAttribute(key, value));
    			String message = arango.createDocument("Reports", reportObject);
                res.put("status", "200");
                res.put("message",new JSONObject().put("id", message));
        	}else
        	{
                res.put("status", "500");
                res.put("message",new JSONObject().put("error", "Error finding post."));
        	}
            
        } catch(Exception e) {
            e.printStackTrace();
			res.put("status", "500");
			res.put("message",new JSONObject().put("error", "Failed to add report to post"));
        }
        return res;
	}
	
	}
