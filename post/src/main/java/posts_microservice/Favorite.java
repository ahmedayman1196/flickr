package posts_microservice;

import command.Command;
import service_locator.ServiceLocator;

import org.json.JSONObject;
import sql_microservice.PostgresService;

public class Favorite implements Command {

    private PostgresService sql_db;

    public Favorite() {
        sql_db = ServiceLocator.getInstance().getServiceByName("pg");
    }

    @Override
    public JSONObject run(JSONObject object) {
        JSONObject res = new JSONObject();
        try {
            JSONObject queryRes = sql_db.execute(String.format("SELECT  favorite_post(%s, %s)", object.get("user_id"), object.get("post_id")));
            if (queryRes == null) {
                res.put("status", "500");
                res.put("message",new JSONObject().put("error", "Failed to favorite post."));
            }
            res.put("status", "200");
            res.put("message",new JSONObject().put("result",true));
        } catch(Exception e) {
            res.put("status", "500");
            res.put("message",new JSONObject().put("error", "Failed to favorite post."));
            e.printStackTrace();
        }
        System.out.println(res);
        return null;
    }

    public static void main(String[] args) {
        new Favorite().run(new JSONObject().put("user_id", 1).put("post_id", 1));
    }
}
