package posts_microservice;

import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Update implements Command {
	private ArangoService arango;
	
	public Update() {
		this.arango = ServiceLocator.getInstance().getServiceByName("arango");
	}
    public String currentTime (){

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return (sdf.format(cal.getTime()) );
    }

    @Override
    public JSONObject run(JSONObject object) {
        JSONObject res = new JSONObject();
        String updateTime = currentTime() ;
        try {
            int postid = object.getInt("post_id");
            String name = object.getString("name");
            JSONArray tagged = object.getJSONArray("tagged");
            JSONArray hashtags = object.getJSONArray("hashtags");
            System.out.println(postid);
            Map<String, Object> bindvars = new HashMap<String, Object>();
            bindvars.put("id", postid);
            bindvars.put("name", name);
            bindvars.put("tagged", tagged);
            bindvars.put("hashtags", hashtags);
            bindvars.put("updated_at", updateTime);
            JSONObject queryRes = arango.query("FOR t IN Posts FILTER t._key == @id "
                    + "UPDATE t WITH {name: @name, hashtags: PUSH(t.hashtags, @hashtags), tagged: PUSH(t.tagged, @tagged)} IN Posts",bindvars);

            res.put("status", "200");
            res.put("message",new JSONObject().put("result",queryRes));
            System.out.println(queryRes);

        } catch (Exception e) {
//			throw e;
            res.put("status", "500");
            res.put("message",new JSONObject().put("error","error finding post"));
        }
        return res;
    }
}
