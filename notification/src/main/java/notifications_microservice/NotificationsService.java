package notifications_microservice;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

import org.json.JSONException;

import command_provider.CommandProvider;
import configuration.AppConfig;
import configuration.Configurations;
import exceptions.ServiceStorageException;
import service_container.ServiceContainer;
import service_locator.ServiceLocator;

public class NotificationsService extends ServiceContainer{

	public NotificationsService(CommandProvider commandProvider, ExecutorService executor
			, int channelCount, int messagePrefetchCount) {
		super("notifications_service", commandProvider, executor, channelCount, messagePrefetchCount);
	}

	public static void main(String[] args) throws ServiceStorageException, ClassNotFoundException, JSONException, IOException {
		Configurations.defaultConfig();
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		AppConfig appConfig = serviceLocator.getServiceByName("config");
		
		new NotificationsService(
				serviceLocator.getServiceByName("commandRegistry"),
				serviceLocator.getServiceByName("executor"),
				Integer.parseInt(appConfig.getConfigVar("NUM_THREADS")),
				Integer.parseInt(appConfig.getConfigVar("MESSAGE_PREFETCH_COUNT"))
		);
	}
}
