create or replace function create_user(
	name varchar,
	birth_date date,
	username varchar,
	email varchar,
	mobile_number varchar,
	gender varchar,
	password varchar
)
returns int as
$$
begin
	insert into users(name, birthdate, username, email, mobile_number, gender, password, created_at)
		values (name, birth_date, username, email, mobile_number, gender, password, now());
	return currval('users_id_seq');
end
$$
	language 'plpgsql';
	
create or replace function get_user(id_in int)
returns setof users as
$$
begin
	return query (select * from users where users.id = id_in);
end
$$
	language 'plpgsql';
	
create or replace function delete_user(id_in int)
returns int as
$$
declare
	ucount int;
begin
	update users set is_deleted = true, updated_at = now() where users.id = id_in and is_deleted = false;
	get diagnostics ucount = ROW_COUNT;
	return ucount;
end
$$
	language 'plpgsql';
	
create or replace function update_user(
	id_in int,
	name_in varchar,
	birth_date_in date,
	mobile_number_in varchar,
	gender_in varchar,
	cover_pic_in varchar,
	profile_pic_in varchar
)
returns int as
$$
declare
	ucount int;
begin
	update users set 
		name = name_in,
		birthdate = birth_date_in,
		mobile_number = mobile_number_in,
		gender = gender_in,
		cover_pic = cover_pic_in,
		profile_pic = profile_pic_in,
		updated_at = now()
		where id = id_in and is_deleted = false;
	get diagnostics ucount = ROW_COUNT;
	return ucount;
end
$$
	language 'plpgsql';
	

create or replace function block_user(blocker_id int, to_block_id int)
returns int as
$$
begin
	insert into users_blocks(user_id, blocked_user_id) select blocker_id, to_block_id where exists(
			select * from users where id = blocker_id and is_deleted = false
		);
	return currval('users_blocks_id_seq');
end
$$
	language 'plpgsql';
	
create or replace function unblock_user(blocker_id int, to_block_id int)
returns int as
$$
declare
	dcount int;
begin
	delete from users_blocks where user_id = blocker_id and blocked_user_id=to_block_id and exists(
			select is_deleted from users where id = blocker_id and is_deleted = false
		);
	get diagnostics dcount = ROW_COUNT;
	return dcount;
end
$$
	language 'plpgsql';
	
create or replace function create_conversation(_first_user_id_in int, _second_user_id_in int)
returns int as
$$
declare
	first_user_id_in int := least(_first_user_id_in, _second_user_id_in);
	second_user_id_in int := greatest(_first_user_id_in, _second_user_id_in); 
begin
	insert into users_conversations(first_user_id, second_user_id) select first_user_id_in, second_user_id_in
	where( 
			(
				select Count(*) from users where ((id = first_user_id_in and is_deleted = false)
					or (id = second_user_id_in and is_deleted=false 
					and not exists(select * from users_blocks where user_id = first_user_id_in and block_id = second_user_id_in)))
			) = 2
		);
	return currval('users_conversations_id_seq');
end
$$
	language 'plpgsql';
	
create or replace function delete_user_report(report_id int, user_id int)
returns int as
$$
declare
	dcount int;
begin
	delete from users_reports where report_id = report_id and user_id = user_id;
	get diagnostics dcount = ROW_COUNT;
	return dcount;
end
$$
language 'plpgsql';

create or replace function follow_user(user_id int, follows_id int)
returns int as
$$
begin
	insert into users_follows(user_id, followed_user_id) select user_id, follows_id where exists(
			select * from users where id = user_id and is_deleted = false
		);
	return currval('users_follows_id_seq');
end
$$
language 'plpgsql';
	
create or replace function unfollow_user(user_to_unfollow_id int, unfollowed_id int)
returns int as
$$
declare
	dcount int;
begin
	delete from users_follows where user_id = user_to_unfollow_id and followed_user_id=unfollowed_id and exists(
			select is_deleted from users where id = user_id and is_deleted = false
		);
	get diagnostics dcount = ROW_COUNT;
	return dcount;
end
$$
	language 'plpgsql';

create or replace function create_message(member_id int, conv_id varchar, message_key varchar)
returns int as
$$
declare
	dcount int;
begin
	insert into conversation_messages(user_id, message_id, conversation_id)
		values (member_id, message_key, conv_id);
	get diagnostics dcount = ROW_COUNT;
	return dcount;
end
$$
language 'plpgsql';
	
create or replace function like_comment(comment_id_in integer, user_id_in integer)
returns int as
$$
begin
	insert into comment_like(comment_id, user_id) values (comment_id_in, user_id_in);
	return currval('comment_like_id_seq');
end
$$
	language 'plpgsql';
	
create or replace function unlike_comment(comment_id_in integer, user_id_in integer)
returns int as
$$
declare
	dcount int;
begin
	delete from comment_like where comment_id = comment_id_in and user_id = user_id;
	get diagnostics dcount = ROW_COUNT;
	return dcount;
end
$$
	language 'plpgsql';

-- Albums Procedures --
-- create album
create or replace function create_album(user_id int, name varchar, description varchar)
returns int as
$$
begin
	insert into albums(user_id, name, description) values(user_id, name, description);
	return currval('albums_id_seq');
end
$$
	language 'plpgsql';

-- retrieve album
CREATE OR REPLACE FUNCTION view_albums(u_id int) 
RETURNS setof albums as 
$$
BEGIN
 RETURN QUERY(SELECT * FROM albums WHERE albums.user_id = u_id); 
END; 
$$ 
 
LANGUAGE 'plpgsql';

--delete album
create or replace function delete_album(album_id int)
returns int as
$$
declare
	dcount int;
begin
	delete from albums where albums.id = album_id;
	get diagnostics dcount = ROW_COUNT;
	return dcount;
end
$$
	language 'plpgsql';

-- favorite album
create or replace function favorite_album(user_id int, album_id int)
returns int as
$$
begin
	insert into users_favorite_albums(user_id, album_id)
		values (user_id, album_id);
	return currval('users_favorite_albums_id_seq');
end
$$
	language 'plpgsql';

-- unfavorite album
create or replace function unfavorite_album(u_id int, a_id int)
returns int as
$$
declare
	dcount int;
begin
	delete from users_favorite_albums where users_favorite_albums.album_id = a_id and users_favorite_albums.user_id = u_id;
	get diagnostics dcount = ROW_COUNT;
	return dcount;
end
$$
	language 'plpgsql';

-- add post
create or replace function add_post(post_id int, album_id int)
returns int as
$$
begin
	insert into album_post(post_id, album_id)
		values (post_id, album_id);
	return currval('album_post_id_seq');
end
$$
	language 'plpgsql';

-- delete post
create or replace function delete_post(p_id int, a_id int)
returns int as
$$
declare
	dcount int;
begin
	delete from album_post where album_post.album_id = a_id and album_post.post_id = p_id;
	get diagnostics dcount = ROW_COUNT;
	return dcount;
end
$$
	language 'plpgsql';

-- retrieve posts
CREATE OR REPLACE FUNCTION view_albums_posts(a_id int) 
RETURNS setof album_post as 
$$
BEGIN
 RETURN QUERY(SELECT * FROM album_post WHERE album_post.album_id = a_id); 
END; 
$$ 
	language 'plpgsql';

-- retrieve favorite albums
CREATE OR REPLACE FUNCTION view_favorite_albums(u_id int) 
RETURNS setof users_favorite_albums as 
$$
BEGIN
 RETURN QUERY(SELECT * FROM users_favorite_albums WHERE users_favorite_albums.user_id = u_id); 
END; 
$$ 
	language 'plpgsql';
--------------------------------------------------------------

---Groups procedures--

--join: user-group relation
create or replace function create_user_group(user_id int, group_id varchar)
returns int as
$$
begin
	insert into users_groups(user_id,group_id)
		values (user_id,group_id);
	return currval('users_groups_id_seq');
end
$$
	language 'plpgsql';

-- Galleries Procedures --
-- create gallery
create or replace function create_gallery(user_id int, name varchar, description varchar)
returns int as
$$
begin
	insert into gallery(user_id, name, description) values(user_id, name, description);
	return currval('gallery_id_seq');
end
$$
	language 'plpgsql';

-- retrieve gallery
CREATE OR REPLACE FUNCTION view_galleries(u_id int) 
RETURNS setof gallery as 
$$
BEGIN
 RETURN QUERY(SELECT * FROM gallery WHERE gallery.user_id = u_id); 
END; 
$$ 
 
LANGUAGE 'plpgsql';

--delete gallery
create or replace function delete_gallery(gallery_id int)
returns int as
$$
declare
	dcount int;
begin
	delete from gallery where gallery.id = gallery_id;
	get diagnostics dcount = ROW_COUNT;
	return dcount;
end
$$
	language 'plpgsql';

--leave: user-group relation
create or replace function delete_user_group(userid int, groupid varchar)
returns int as
$$
declare
	dcount int;
begin
	delete from users_groups where users_groups.group_id = groupid and users_groups.user_id = userid;
	get diagnostics dcount = ROW_COUNT;
	return dcount;
end
$$
	language 'plpgsql';
-- favorite gallery
create or replace function favorite_gallery(user_id int, gallery_id int)
returns int as
$$
begin
	insert into users_favorite_gallery(user_id, gallery_id)
		values (user_id, gallery_id);
	return currval('users_favorite_gallery_id_seq');
end
$$
	language 'plpgsql';

-- unfavorite gallery
create or replace function unfavorite_gallery(u_id int, g_id int)
returns int as
$$
declare
	dcount int;
begin
	delete from users_favorite_gallery where users_favorite_gallery.gallery_id = g_id and users_favorite_gallery.user_id = u_id;
	get diagnostics dcount = ROW_COUNT;
	return dcount;
end
$$
	language 'plpgsql';

--get user by name
create or replace function get_user_by_name(users_name varchar)
returns setof users as
$$
begin
	return query (select * from users where users.name LIKE users_name);
end
$$
	language 'plpgsql';

create or replace function get_user_by_email(user_email varchar)
returns setof users as
$$
begin
	return query (select * from users where users.email LIKE user_email);
end
$$
	language 'plpgsql';

create unique index gallery_post_idx on gallery_post(post_id, gallery_id);

ALTER TABLE public.gallery_post
    OWNER to postgres;

-- add post
create or replace function add_post_gallery(post_id int, gallery_id int)
returns int as
$$
begin
	insert into gallery_post(post_id, gallery_id)
		values (post_id, gallery_id);
	return currval('gallery_post_id_seq');
end
$$
	language 'plpgsql';

-- delete post
create or replace function delete_post_gallery(p_id int, g_id int)
returns int as
$$
declare
	dcount int;
begin
	delete from gallery_post where gallery_post.gallery_id = g_id and gallery_post.post_id = p_id;
	get diagnostics dcount = ROW_COUNT;
	return dcount;
end
$$
	language 'plpgsql';

-- retrieve posts
CREATE OR REPLACE FUNCTION view_gallery_posts(g_id int) 
RETURNS setof gallery_post as 
$$
BEGIN
 RETURN QUERY(SELECT * FROM gallery_post WHERE gallery_post.gallery_id = g_id); 
END; 
$$ 
	language 'plpgsql';

-- retrieve favorite galleries
CREATE OR REPLACE FUNCTION view_favorite_galleries(u_id int) 
RETURNS setof users_favorite_gallery as 
$$
BEGIN
 RETURN QUERY(SELECT * FROM users_favorite_gallery WHERE users_favorite_gallery.user_id = u_id); 
END; 
$$ 
	language 'plpgsql';

-- retrieve gallery by name
CREATE OR REPLACE FUNCTION search_galleries(n varchar) 
RETURNS setof gallery as 
$$
BEGIN
 RETURN QUERY(SELECT * FROM gallery WHERE gallery.name = n); 
END; 
$$ 	
	language 'plpgsql';


-- add user to conversation
create or replace function add_user_to_conversation(member_id int, conv_id varchar)
returns int as
$$
begin
	insert into users_conversations(user_id, conversation_id) select member_id, conv_id where exists(
			select * from users where id = member_id and is_deleted = false
		);
	return currval('users_conversations_users_id');
end
$$
	language 'plpgsql';


CREATE OR REPLACE FUNCTION get_conversation(member1_id int, member2_id int) 
RETURNS setof varchar as 
$$
BEGIN
 RETURN QUERY (SELECT m.conversation_id FROM users_conversations e INNER JOIN users_conversations m ON CAST(m.conversation_id as int) = CAST(e.conversation_id as int) and m.user_id = member1_id  and e.user_id =  member2_id LIMIT 1); 
END; 
$$ 	
	language 'plpgsql';

	