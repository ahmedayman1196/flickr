-- Table: public.users

-- DROP TABLE public.users;

CREATE TABLE public.users
(
    id serial primary key,
    name character varying(100) COLLATE pg_catalog."default",
    birthdate date,
    username character varying(100) COLLATE pg_catalog."default",
    email character varying(100) COLLATE pg_catalog."default",
    mobile_number varchar,
    gender character varying(100) COLLATE pg_catalog."default",
    password character varying(100) COLLATE pg_catalog."default",
    active_notifications boolean,
    cover_pic character varying(1000) COLLATE pg_catalog."default",
    profile_pic character varying(1000) COLLATE pg_catalog."default",
    is_deleted boolean default false,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
)
WITH (
    OIDS = TRUE
)
TABLESPACE pg_default;

create unique index users_email on users(email);
create unique index users_username on users(username);
ALTER TABLE public.users
    OWNER to postgres;



-- Table: public.users_blocks

-- DROP TABLE public.users_blocks;

CREATE TABLE public.users_blocks
(
    user_id int NOT NULL,
    blocked_user_id int NOT NULL,
    id serial NOT NULL,
    foreign key (user_id) references users(id),
    foreign key (blocked_user_id) references users(id),
    CONSTRAINT users_blocks_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

create unique index users_blocks_user_to_user_idx on users_blocks(user_id, blocked_user_id);

ALTER TABLE public.users_blocks
    OWNER to postgres;


-- Table: public.users_conversations

-- Drop table public.users_conversations

CREATE TABLE public.users_conversations
(
    id serial,
    user_id int NOT NULL,
    foreign key (user_id) references users,
    conversation_id varchar NOT null,
    CONSTRAINT users_conversations_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

create unique index users_conversations_users_id on users_conversations(user_id, conversation_id);

ALTER TABLE public.users_conversations
    OWNER to postgres;


-- Table: public.conversation_messages

-- Drop table public.conversation_messages

CREATE TABLE public.conversation_messages
(
    id serial,
    user_id int NOT null,
    message_id varchar NOT null,
    conversation_id varchar NOT null,
    foreign key (user_id) references users,
    CONSTRAINT conversation_messages_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.conversation_messages
    OWNER to postgres;


-- Table: public.users_groups

-- DROP TABLE public.users_groups;

CREATE TABLE public.users_groups
(
    user_id int not null,
	group_id varchar NOT null,
    id serial primary key,
    foreign key (user_id) references users(id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

create unique index users_groups_users_id on users_groups(user_id, group_id);

ALTER TABLE public.users_groups
    OWNER to postgres;

-- Table: public.users_favorite_posts

-- DROP TABLE public.users_favorite_posts;

CREATE TABLE public.users_favorite_posts
(
    user_id integer NOT NULL,
    post_id varchar NOT null,
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default
;create unique index users_favorite_posts_users_id on users_favorite_posts(user_id, post_id);


ALTER TABLE public.users_favorite_posts
    OWNER to postgres;

-- Table: public.users_favorite_galleries

-- DROP TABLE public.users_favorite_galleries;

CREATE TABLE public.users_favorite_galleries
(
    user_id integer NOT NULL,
    gallery_id varchar NOT null,
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

;create unique index users_favorite_galleries_users_id on users_favorite_galleries(user_id, gallery_id);


ALTER TABLE public.users_favorite_galleries
    OWNER to postgres;

-- Table: public.users_follows

-- DROP TABLE public.users_follows;

CREATE TABLE public.users_follows
(
    user_id int NOT NULL,
    followed_user_id int NOT NULL,
    id serial NOT NULL,
    foreign key (user_id) references users(id),
    foreign key (followed_user_id) references users(id),
    CONSTRAINT users_follows_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

create unique index users_follows_user_to_user_idx on users_follows(user_id, followed_user_id);

ALTER TABLE public.users_follows
    OWNER to postgres;

-- Table: public.comments_likes
create table public.comments_likes
(
	id serial primary key,
	comment_id varchar NOT null,
	user_id int not null,
	foreign key (user_id) references users(id)
);

create index comments_likes_comment_id on comments_likes(user_id);

create unique index comments_likes_comment_user on comments_likes(comment_id, user_id);

ALTER TABLE public.comments_likes
    OWNER to postgres;

-- Table: public.discussions_likes
create table public.discussions_likes
(
	id serial primary key,
    discussion_id varchar NOT null,
	user_id int not null,
	foreign key (user_id) references users(id)
);

create index discussions_likes_discussion_id on discussions_likes(user_id);

create unique index discussions_likes_discussion_user on discussions_likes(discussion_id, user_id);

ALTER TABLE public.discussions_likes
    OWNER to postgres;

-- Table: public.posts_likes
create table public.posts_likes
(
	id serial primary key,
    post_id varchar NOT null,
	user_id int not null,
	foreign key (user_id) references users(id)
);

create index posts_likes_post_id on posts_likes(user_id);

create unique index posts_likes_post_user on posts_likes(post_id, user_id);

ALTER TABLE public.posts_likes
    OWNER to postgres;


-- Table: public.posts_hashtags

-- DROP TABLE public.posts_hashtags;

CREATE TABLE public.posts_hashtags
(
    user_id int,
	hashtag_id varchar NOT null,
    id serial primary key,
    foreign key (user_id) references users(id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.posts_hashtags
    OWNER to postgres;

-- Table: public.posts_tags

-- DROP TABLE public.posts_tags;

CREATE TABLE public.posts_tags
(
    tagged_user_id int not null,
	post_id varchar NOT null,
    id serial primary key,
    foreign key (tagged_user_id) references users(id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.posts_tags
    OWNER to postgres;

-- Table: public.groups_posts
CREATE TABLE public.groups_posts
(
	id serial NOT NULL,
    post_id varchar NOT null,
	group_id varchar NOT null,
    CONSTRAINT groups_posts_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

create unique index groups_posts_idx on groups_posts(post_id, group_id);

ALTER TABLE public.groups_posts
    OWNER to postgres;


-- Albums
-- Table: public.albums
CREATE TABLE public.albums
(
    name character varying(100) COLLATE pg_catalog."default",
    description character varying(500) COLLATE pg_catalog."default",
    user_id int NOT NULL,
    id serial NOT NULL,
	foreign key (user_id) references users(id),
    CONSTRAINT albums_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.albums
    OWNER to postgres;


-- Table: public.users_favorite_albums
CREATE TABLE public.users_favorite_albums
(
    user_id int NOT NULL,
    album_id int NOT NULL,
    id serial NOT NULL,
    foreign key (user_id) references users(id),
    foreign key (album_id) references albums(id),
    CONSTRAINT users_favorite_albums_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.users_favorite_albums
    OWNER to postgres;

-- Table: public.album_post
CREATE TABLE public.album_post
(
	id serial NOT NULL,
    post_id int NOT null,
	album_id int NOT null,
    foreign key (album_id) references albums(id),
    CONSTRAINT album_post_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

create unique index album_post_idx on album_post(post_id, album_id);

ALTER TABLE public.album_post
    OWNER to postgres;


-- Gallaries
CREATE TABLE public.gallery
(
    name character varying(100) COLLATE pg_catalog."default",
    description character varying(500) COLLATE pg_catalog."default",
    user_id int NOT NULL,
    id serial NOT NULL,
    foreign key (user_id) references users(id),
    CONSTRAINT gallery_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.gallery
    OWNER to postgres;
	
CREATE TABLE public.users_favorite_gallery
(
    user_id int NOT NULL,
    gallery_id int NOT NULL,
    id serial NOT NULL,
    foreign key (user_id) references users(id),
    foreign key (gallery_id) references gallery(id),
    CONSTRAINT users_favorite_gallery_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.users_favorite_gallery
    OWNER to postgres;

-- Table: public.gallery_post
CREATE TABLE public.gallery_post
(
	id serial NOT NULL,
    post_id int NOT null,
	gallery_id int NOT null,
    foreign key (gallery_id) references gallery(id),
    CONSTRAINT gallery_post_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
