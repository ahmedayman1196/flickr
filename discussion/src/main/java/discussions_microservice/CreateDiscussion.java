package discussions_microservice;

import org.json.JSONObject;

import com.arangodb.entity.BaseDocument;

import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

public class CreateDiscussion implements Command {

	private ArangoService arango;
	
	public CreateDiscussion() {
		this.arango = ServiceLocator.getInstance().getServiceByName("arango");
	}
	
	public JSONObject run(JSONObject request) {
		JSONObject response = new JSONObject();
		//Authentication Still needed
		try{
			JSONObject user = (JSONObject) request.get("user");
			JSONObject dicussion = (JSONObject) request.get("discussion");
			BaseDocument discObj = new BaseDocument();
	
			discObj.addAttribute("owner", user.getString("id"));
			discObj.addAttribute("title", dicussion.getString("title"));
			discObj.addAttribute("post_text", dicussion.getString("post_text"));
			discObj.addAttribute("group_id", dicussion.getString("group_id"));
			discObj.addAttribute("status", dicussion.getBoolean("status"));

			try {
				String discID = arango.createDocument("Discussions", discObj);
				response.put("status", "200");
				response.put("message", new JSONObject().put("result", discID));
			}catch(Exception e) {
				System.err.println(e.getMessage());
				response.put("status", "500");
				response.put("message", new JSONObject().put("error", "Database error."));
			}
		} catch (Exception e) {
			response.put("status", "400");
			response.put("message", new JSONObject().put("error", "Missing param(s)."));
		}

		return response;

	}
}
